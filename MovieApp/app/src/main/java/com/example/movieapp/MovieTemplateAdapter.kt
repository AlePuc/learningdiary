package com.example.movieapp

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class MovieTemplateAdapter(var exampleMovies : ArrayList<Movie>) : RecyclerView.Adapter<MovieTemplateAdapter.MovieTemplateViewHolder>() {

    private lateinit var clickListener : OnItemClickListener


    interface OnItemClickListener{
        fun showDetails(position:Int)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener){
        clickListener =  onItemClickListener
    }


    inner class MovieTemplateViewHolder(itemView: View, listener: OnItemClickListener) : RecyclerView.ViewHolder(itemView) {
        var movieTitle: TextView = itemView.findViewById(R.id.template_movieTitle)
        var movieCreator : TextView = itemView.findViewById(R.id.template_movieCreators)
        var movieActor : TextView = itemView.findViewById(R.id.template_movieActors)
        var detailButton : Button = itemView.findViewById(R.id.template_detailButton)

        init {
            setOnClickListeners(listener)
        }


        private fun setOnClickListeners(listener: OnItemClickListener){
            showDetails(listener)
        }


        private fun showDetails(listener: OnItemClickListener){
            detailButton.setOnClickListener {
                val position = adapterPosition
                if(position != RecyclerView.NO_POSITION){
                    listener.showDetails(position)
                }
            }
        }

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieTemplateViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.template_moviecard,parent,false)
        return MovieTemplateViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int {
        return exampleMovies.size
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: MovieTemplateViewHolder, position: Int) {
        val movie = exampleMovies[position]
        setMovieTitle(movie,holder)
        setMovieCreator(movie,holder)
        setMovieActor(movie,holder)
    }

    private fun setMovieTitle(movie:Movie,holder:MovieTemplateViewHolder){
        holder.movieTitle.text = movie.title
    }

    private fun setMovieCreator(movie:Movie,holder:MovieTemplateViewHolder){
        holder.movieCreator.text = movie.creators
    }

    private fun setMovieActor(movie:Movie,holder:MovieTemplateViewHolder){
        holder.movieActor.text = movie.actors
    }

}