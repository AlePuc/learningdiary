package com.example.movieapp

interface Communicator {

    fun passDataToDetailFragment(position:Int)
    fun passNoDataToWatchlist()
}