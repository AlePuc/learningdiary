package com.example.movieapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.movieapp.databinding.FragmentDetailBinding



class DetailFragment : Fragment() {
    private lateinit var binding:FragmentDetailBinding
    private lateinit var sharedViewModelMovie: SharedViewModelMovie

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(false)

        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_detail, container, false
            )

        sharedViewModelMovie = ViewModelProviders.of(requireActivity()).get(SharedViewModelMovie::class.java)

        val movie = getMovieFromArguments()

        binding.movie = movie

        binding.movieRating.rating = movie.rating

        binding.movieAdd.setOnClickListener {
            sharedViewModelMovie.addMovieToWatchlist(movie)
            val toast = Toast.makeText(context,"${movie.title} added to watchlist", Toast.LENGTH_SHORT)
            toast.show()
        }
        return binding.root
    }

    private fun getMovieFromArguments():Movie{
        val position = arguments?.getInt("position")
        return sharedViewModelMovie.getMovie(position!!)
    }
    companion object {

    }
}
