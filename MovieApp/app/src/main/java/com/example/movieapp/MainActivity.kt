package com.example.movieapp

import androidx.navigation.findNavController
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.movieapp.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(),Communicator{

    private lateinit var mSharedViewModelMovie: SharedViewModelMovie
    private lateinit var binding: ActivityMainBinding

    private lateinit var toggle : ActionBarDrawerToggle
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this,com.example.movieapp.R.layout.activity_main
            )

        drawerLayout = binding.drawerLayout
        toggle = ActionBarDrawerToggle(this,drawerLayout,R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.navView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.deleteWatchlist -> Toast.makeText(applicationContext,"Watchlist cleared",Toast.LENGTH_SHORT).show()
                R.id.showWatchlist -> Toast.makeText(applicationContext,"Watchlist cleared",Toast.LENGTH_SHORT).show()
            }
            true
        }

        val movie1 = Movie(
            "The Queen's Gambit1",
            "The Queen's Gambit follows the life of an orphan chess prodigy, Elizabeth Harmon, during her quest to become the world's greatest chess player while struggling with emotional problems, drugs and alcohol dependency. The title of the series refers to a chess opening of the same name. The story begins in the mid-1950s and proceeds into the 1960s.[4]\n" +
                    "The story begins in Lexington, Kentucky, where a nine-year-old Beth, having lost her mother in a car crash, is taken to an orphanage where she is taught chess by the building's custodian, Mr. Shaibel. As was common during the 1950s, the orphanage dispenses daily tranquilizer pills to the girls,[5][6] which turns into an addiction for Beth. She quickly becomes a strong chess player due to her visualization skills, which are enhanced by the tranquilizers. A few years later, Beth is adopted by Alma Wheatley and her husband from Lexington. As she adjusts to her new home, Beth enters a chess tournament and wins despite having no prior experience in competitive chess. She develops friendships with several people, including former Kentucky State Champion Harry Beltik, United States National Champion Benny Watts, and journalist and fellow player D.L. Townes. As Beth rises to the top of the chess world and reaps the financial benefits of her success, her drug and alcohol dependency becomes worse.\n",
            3.4f,
            "Drama, Sport",
            "Scott Frank, Alan Scott",
            "Anya Taylor-Joy, Chloe Pirrie"
        )

        val movie2 = Movie(
            "The Queen's Gambit2",
            "The Queen's Gambit follows the life of an orphan chess prodigy, Elizabeth Harmon, during her quest to become the world's greatest chess player while struggling with emotional problems, drugs and alcohol dependency. The title of the series refers to a chess opening of the same name. The story begins in the mid-1950s and proceeds into the 1960s.[4]\n" +
                    "The story begins in Lexington, Kentucky, where a nine-year-old Beth, having lost her mother in a car crash, is taken to an orphanage where she is taught chess by the building's custodian, Mr. Shaibel. As was common during the 1950s, the orphanage dispenses daily tranquilizer pills to the girls,[5][6] which turns into an addiction for Beth. She quickly becomes a strong chess player due to her visualization skills, which are enhanced by the tranquilizers. A few years later, Beth is adopted by Alma Wheatley and her husband from Lexington. As she adjusts to her new home, Beth enters a chess tournament and wins despite having no prior experience in competitive chess. She develops friendships with several people, including former Kentucky State Champion Harry Beltik, United States National Champion Benny Watts, and journalist and fellow player D.L. Townes. As Beth rises to the top of the chess world and reaps the financial benefits of her success, her drug and alcohol dependency becomes worse.\n",
            3.4f,
            "Drama, Sport",
            "Scott Frank, Alan Scott",
            "Anya Taylor-Joy, Chloe Pirrie"
        )

        val movie3 = Movie(
            "The Queen's Gambit3",
            "The Queen's Gambit follows the life of an orphan chess prodigy, Elizabeth Harmon, during her quest to become the world's greatest chess player while struggling with emotional problems, drugs and alcohol dependency. The title of the series refers to a chess opening of the same name. The story begins in the mid-1950s and proceeds into the 1960s.[4]\n" +
                    "The story begins in Lexington, Kentucky, where a nine-year-old Beth, having lost her mother in a car crash, is taken to an orphanage where she is taught chess by the building's custodian, Mr. Shaibel. As was common during the 1950s, the orphanage dispenses daily tranquilizer pills to the girls,[5][6] which turns into an addiction for Beth. She quickly becomes a strong chess player due to her visualization skills, which are enhanced by the tranquilizers. A few years later, Beth is adopted by Alma Wheatley and her husband from Lexington. As she adjusts to her new home, Beth enters a chess tournament and wins despite having no prior experience in competitive chess. She develops friendships with several people, including former Kentucky State Champion Harry Beltik, United States National Champion Benny Watts, and journalist and fellow player D.L. Townes. As Beth rises to the top of the chess world and reaps the financial benefits of her success, her drug and alcohol dependency becomes worse.\n",
            3.4f,
            "Drama, Sport",
            "Scott Frank, Alan Scott",
            "Anya Taylor-Joy, Chloe Pirrie"
        )

        val movie4 = Movie(
            "The Queen's Gambit4",
            "The Queen's Gambit follows the life of an orphan chess prodigy, Elizabeth Harmon, during her quest to become the world's greatest chess player while struggling with emotional problems, drugs and alcohol dependency. The title of the series refers to a chess opening of the same name. The story begins in the mid-1950s and proceeds into the 1960s.[4]\n" +
                    "The story begins in Lexington, Kentucky, where a nine-year-old Beth, having lost her mother in a car crash, is taken to an orphanage where she is taught chess by the building's custodian, Mr. Shaibel. As was common during the 1950s, the orphanage dispenses daily tranquilizer pills to the girls,[5][6] which turns into an addiction for Beth. She quickly becomes a strong chess player due to her visualization skills, which are enhanced by the tranquilizers. A few years later, Beth is adopted by Alma Wheatley and her husband from Lexington. As she adjusts to her new home, Beth enters a chess tournament and wins despite having no prior experience in competitive chess. She develops friendships with several people, including former Kentucky State Champion Harry Beltik, United States National Champion Benny Watts, and journalist and fellow player D.L. Townes. As Beth rises to the top of the chess world and reaps the financial benefits of her success, her drug and alcohol dependency becomes worse.\n",
            3.4f,
            "Drama, Sport",
            "Scott Frank, Alan Scott",
            "Anya Taylor-Joy, Chloe Pirrie"
        )

        val movies = ArrayList<Movie>()
        movies.add(movie1)
        movies.add(movie2)
        movies.add(movie3)
        movies.add(movie4)

        mSharedViewModelMovie = ViewModelProviders.of(this).get(SharedViewModelMovie::class.java)
        mSharedViewModelMovie.setMovies(movies)




        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(android.R.id.content, HomeFragment())
        ft.commit()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun passNoDataToWatchlist() {
        val transaction = this.supportFragmentManager.beginTransaction()
        val fragmentWatchlist = WatchlistFragment()

        transaction.add(android.R.id.content, fragmentWatchlist).addToBackStack("")
        transaction.commit()
    }

    override fun passDataToDetailFragment(position: Int) {
       val bundle = Bundle()
        bundle.putInt("position", position)


        val transaction = this.supportFragmentManager.beginTransaction()
        val fragmentDetail = DetailFragment()
        fragmentDetail.arguments = bundle

        transaction.add(android.R.id.content, fragmentDetail).addToBackStack("")
        transaction.commit()
    }
}
