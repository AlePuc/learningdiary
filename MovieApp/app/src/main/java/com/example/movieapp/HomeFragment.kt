package com.example.movieapp

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding

    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: MovieTemplateAdapter
    private lateinit var recyclerViewLayoutManager: LinearLayoutManager
    private lateinit var sharedViewModelMovie: SharedViewModelMovie

    private lateinit var communicator: Communicator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home, container, false
        )

        communicator = activity as Communicator

        setHasOptionsMenu(true)

        // Inflate the layout for this fragment
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModelMovie = ViewModelProviders.of(requireActivity()).get(SharedViewModelMovie::class.java)
        buildRecycleView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.app_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.deleteWatchlist -> {
                sharedViewModelMovie.restWatchlist()
            }
            R.id.watchlistFragment ->{
                communicator.passNoDataToWatchlist()
            }
        }

        return true
    }

    private fun buildRecycleView() {
        recyclerView = binding.recyclerView
        recyclerView.hasFixedSize()

        recyclerViewLayoutManager = LinearLayoutManager(context)
        recyclerViewAdapter = MovieTemplateAdapter(sharedViewModelMovie.getMovies())

        recyclerView.adapter = recyclerViewAdapter
        recyclerView.layoutManager = recyclerViewLayoutManager

        val obj = object :
            MovieTemplateAdapter.OnItemClickListener {
            override fun showDetails(position: Int) {
                sharedViewModelMovie.setPosition(position)
                communicator.passDataToDetailFragment(position)
            }
        }
        recyclerViewAdapter.setOnItemClickListener(obj)
    }


    companion object {

    }
}
