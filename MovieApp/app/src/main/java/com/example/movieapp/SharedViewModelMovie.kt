package com.example.movieapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModelMovie : ViewModel() {

    private var movies = MutableLiveData<ArrayList<Movie>>()
    private var position:MutableLiveData<Int>? =  MutableLiveData()
    private var watchlist = MutableLiveData<ArrayList<Movie>>()

    fun setMovies(movies: ArrayList<Movie>){
        this.movies.value = ArrayList()
        for(movie: Movie in movies){
            this.movies.value!!.add((movie))
        }
    }

    fun getMovie(position:Int):Movie{
        if(movies.value ==null){
            movies.value = ArrayList()
        }
        return movies.value!![position]
    }

    fun getMovies():ArrayList<Movie>{
        if(movies.value ==null){
            movies.value = ArrayList()
        }
        return movies.value!!
    }

    fun getSelectedMovie(): Movie {
        if(movies.value ==null) movies.value = ArrayList()
        return movies.value!![this.position!!.value!!]
    }

    fun setPosition(position: Int){
        this.position!!.value = position
    }

    fun getPosition(): Int? {
        return if(this.position != null){
            this.position!!.value
        }else{
            null
        }
    }

    fun getWatchlist():ArrayList<Movie>{
        if(watchlist.value ==null){
            watchlist.value = ArrayList()
        }
        return watchlist.value!!
    }

    fun restWatchlist():ArrayList<Movie>{
        watchlist.value = ArrayList()
        return watchlist.value!!
    }

    fun addMovieToWatchlist(movieToAdd: Movie){
        if(watchlist.value ==null){
            watchlist.value = ArrayList()
        }
        if(!watchlist.value!!.contains(movieToAdd)){
            watchlist.value?.add(movieToAdd)
        }
    }
}